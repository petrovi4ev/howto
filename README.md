1. [Очереди в Yii2](https://gitlab.com/PetrovichevSergey/howto/blob/master/yii2.queue.md).  
2. [Создание собственного PHP пакета](https://gitlab.com/PetrovichevSergey/howto/blob/master/php.package.md).  
3. [Решение проблемы при отсутствии pubkey при обновлении с использованием apt](https://gitlab.com/PetrovichevSergey/howto/blob/master/NO_PUBKEY.md).  