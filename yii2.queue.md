Использование очередей в Yii2.

1. Установка пакета:
    ```bash
    composer require --prefer-dist yiisoft/yii2-queue
    ```
2. Регистрация в конфиге:
    ```php
    <?php
    return [
        'components' => [
            'queue' => [
                'class' => \yii\queue\db\Queue::class,
                'as log' => \yii\queue\LogBehavior::class,
                'mutex' => \yii\mutex\MysqlMutex::class,
            ],
            'bootstrap' => [
                'queue', // Компонент регистрирует свои консольные команды
            ],
        ]
    ];
    ```
3. Создание джоба (например в common/jobs):
    ```php
    <?php
   
    namespace common\jobs;
    
    use common\helpers\BitrixHelper;
    use common\models\Orders;
    use Exception;
    use yii\base\BaseObject;
    use yii\queue\JobInterface;
    
    class NewOrder extends BaseObject implements JobInterface
    {
        /**
         * @var Orders $model
         */
        public $model;
    
        public function execute($queue)
        {
            try {
                $bitrix = new BitrixHelper();
                $bitrix->createDeal($this->model);
    
                echo PHP_EOL . "Order shipped to bitrix" . PHP_EOL;
            } catch (Exception $exception) {
                echo $exception->getMessage() . PHP_EOL;
                echo $exception->getTraceAsString() . PHP_EOL;
            }
        }
    }
    ```
4. Добавление заданий в очередь:
    ```php
    <?php
    
    Yii::$app->queue->push(new NewOrder([
        'model' => $model
    ]));
    ```
5. Выполнение всех заданий очереди в терминале:
    ```bash
    ./yii queue/run
    ```