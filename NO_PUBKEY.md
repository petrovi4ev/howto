## Problem:

```bash
W: Не удалось получить https://mega.nz/linux/MEGAsync/xUbuntu_18.04/./InRelease  Следующие подписи не могут быть проверены, так как недоступен открытый ключ: NO_PUBKEY 03C3AD3A7F068E5D
```

## Solving:

```bash
sudo apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 03C3AD3A7F068E5D
```



